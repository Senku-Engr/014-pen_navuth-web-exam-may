import React, { Component } from "react";
const bluee = {
  backgroundColor: "Blue",
};
const myred = {
  backgroundColor: "red",
};
const myyellow = {
  backgroundColor: "yellow",
};
export default class Book extends Component {
  change = () => {
    this.props.change(this.props.m.id);
  };
  render(props) {
    return (
      <tr style={this.props.m.isHiding === false ? null : bluee}>
        <td>{this.props.m.id}</td>
        <td>{this.props.m.title}</td>
        <td>{this.props.m.publishedYear}</td>
        <td>
          <button
            onClick={this.change}
            style={this.props.m.isHiding === false ? myyellow : myred}
          >
            {this.props.m.isHiding === false ? "hide" : "Unhide"}
          </button>
        </td>
      </tr>
    );
  }
}
