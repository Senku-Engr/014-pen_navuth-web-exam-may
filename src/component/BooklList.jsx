import React, { Component } from "react";
import Book from "./Book";
const mytable = {
  width: "100%",
  padding: "20px",
  textAlign: "center",
};
export default class BooklList extends Component {
  constructor() {
    super();
    this.state = {
      books: [
        {
          id: 1,
          title: "មនុស%ពីរនក់េនផទះជិតគន ",
          publishedYear: 2012,
          isHiding: false,
        },
        { id: 2, title: "គង8់ ៊ ន", publishedYear: 2015, isHiding: false },
        {
          id: 3,
          title: "បុរសេចះថនំពិសពស់",
          publishedYear: 2018,
          isHiding: false,
        },
        { id: 4, title: "អេណ@AកនិងBC", publishedYear: 2019, isHiding: false },
      ],
    };
  }
  change = (id) => {
    this.setState((prevState) => {
      return (this.state.books[id - 1].isHiding = !prevState.books[id - 1]
        .isHiding);
    });
  };

  render() {
    const s = this.state.books.map((m) => (
      <Book m={m} change={this.change} key={m.id} />
    ));
    return (
      <div>
        <table style={mytable}>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Publish year</th>
            <th>Action</th>
          </tr>
          {s}
        </table>
      </div>
    );
  }
}
